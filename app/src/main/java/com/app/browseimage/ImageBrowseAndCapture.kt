package com.app.browseimage
import android.app.Activity
import android.content.ContextWrapper
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class ImageBrowseAndCapture : AppCompatActivity() {

    private var galleryImageUri: Uri? = null
    private lateinit var imageView: ImageView
    private lateinit var imageViewTwo: ImageView
    var bitmapImage: Bitmap? = null
    var imageURL: String? = null
    var cameraImageUrl: String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        imageView = findViewById(R.id.imageView)
        imageViewTwo = findViewById(R.id.imageViewTwo)

        showDialogOne()
        imageView.setOnClickListener {
            val bitmapDrawable = imageView.drawable as BitmapDrawable?
            bitmapImage = bitmapDrawable?.bitmap
            val calender = Calendar.getInstance()
            val lo = calender.timeInMillis
            val imageFileName = "$lo.png"
            savePhoto(bitmapImage, imageFileName)
        }
    }


    private fun showDialogOne() {
        val dialog = BottomSheetDialog(this)
        dialog.setContentView(R.layout.sample_dialog_one)
        val tvGallery = dialog.findViewById<TextView>(R.id.tvGallery)
        val tvCamera = dialog.findViewById<TextView>(R.id.tvCamera)
        tvGallery?.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                    requestGalleryPermissionLauncher.launch(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    dialog.dismiss()
                } else {
                    dialog.dismiss()
                    chooseImageGallery()
                }
            } else {
                dialog.dismiss()
                chooseImageGallery()
            }
        }
        tvCamera?.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
                   requestCameraPermissionLauncher.launch(android.Manifest.permission.CAMERA)
                    dialog.dismiss()
                } else {
                    dialog.dismiss()
                    openCamera()
                }
            } else {
                dialog.dismiss()
                openCamera()
            }
        }
        dialog.show()
    }

    private fun openCamera() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (e: java.lang.Exception) {
            }
            if (photoFile != null) {
                val photoURI = FileProvider.getUriForFile(
                    this,
                    "com.app.browseimage.provider",
                    photoFile
                )
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                cameraLauncher.launch(takePictureIntent)
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File? {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
            imageFileName,
            ".jpg",
            storageDir
        )
        cameraImageUrl = image.absolutePath
        return image
    }

    private val requestCameraPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                openCamera()
            } else {
                Toast.makeText(applicationContext, "Camera Permission Declined", Toast.LENGTH_LONG)
                    .show()
            }
        }


    private val requestGalleryPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                chooseImageGallery()
            } else {
                Toast.makeText(applicationContext, "Gallery Permission Declined", Toast.LENGTH_LONG)
                    .show()
            }
        }

    private fun chooseImageGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        galleryLauncher.launch(intent)
    }


    private var galleryLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                galleryImageUri = result.data?.data
                imageView.setImageURI(galleryImageUri)
            }
        }

    private var cameraLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                Glide.with(this)
                    .load(cameraImageUrl)
                    .into(imageView);
            }
        }

    private fun savePhoto(bitmapImageFile: Bitmap?, fileName: String) {
        val cw = ContextWrapper(applicationContext)
        val directory = cw.getDir("imageDir", MODE_PRIVATE)
        val myPath = File(directory, fileName)
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(myPath)
            bitmapImageFile?.compress(Bitmap.CompressFormat.PNG, 100, fos)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fos!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        imageURL = directory.absolutePath + "/" + fileName
        Toast.makeText(applicationContext, imageURL, Toast.LENGTH_LONG).show()
        Glide.with(this)
            .load(imageURL)
            .into(imageViewTwo);
    }


}


